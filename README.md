# README #

Based on my experience so far with Android I decided to work on Movie app with following features:

* Allows user to search for new and popular movies
* Find theaters nearby playing those movies
* Invite friends to watch movie
* Bookmark and set reminders for upcoming movies
* Search movies by name or genre

This app is still under development.

![Navigation_Drawer_Screen.png](https://bitbucket.org/repo/L9E8E6/images/3056775136-Navigation_Drawer_Screen.png)

![Home Screen.png](https://bitbucket.org/repo/L9E8E6/images/612590491-Home%20Screen.png)